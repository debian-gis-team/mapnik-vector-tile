#!/usr/bin/python3

try:
    from osgeo import gdal
except:
    import gdal
    
tiff = 'test/natural_earth.tif'
tiff_width = 1048
tiff_height = 1048
extent = (-20037507.161367048, 20037508.342789248, -20037508.34278927, 18428920.01277216)

x_res = (extent[1] - extent[0]) / tiff_width
y_res = (extent[3] - extent[2]) / tiff_height

raster = gdal.GetDriverByName('GTiff')
dest = raster.Create(tiff, tiff_width, tiff_height, 4, gdal.GDT_Byte, options = [ 'COMPRESS=JPEG' ])

raster_transform = [extent[0], x_res, 0.0, extent[3], 0.0, -y_res]
dest.SetGeoTransform(raster_transform)

dest.GetRasterBand(1).Fill(0)
dest.GetRasterBand(2).Fill(0)
dest.GetRasterBand(3).Fill(0)
dest.GetRasterBand(4).Fill(255)

